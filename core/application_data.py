from models.delivery_package import DeliveryPackage
from models.avalaible_destinations import AvailableDestiNation
from datetime import datetime, timedelta
from models.company_trucks import Actros, Scania, Man

class ApplicationData:
    def __init__(self):
        self._check: list[DeliveryPackage] = []
        self._scania_truck: list[Scania] = []
        self._test = []
        self._valueses = []
        self._total_weight = 0
        self._total_km_package = []

    def create_delivery_package(self, unique_id, start_location, end_location, weight, contact) -> DeliveryPackage:
        dev_pack = DeliveryPackage(unique_id,
                                   start_location, end_location, weight, contact)
        self._check.append(dev_pack)
        self._total_weight += weight
        return dev_pack

    def check_time_length(self, distance, avg_speed: float):
        """
        Args:
            distance (float): take the current distance
            avg_speed (float): takes the pre-fixed avg speed

        Returns:
            The estimated time that he truck will arrive
        """
        time = distance/avg_speed
        time = str(time)
        time.split(".")

        result_hours = '{0:02.0f}'.format(*divmod(float(time) * 60, 60))
        result_mins = '{1:02.0f}'.format(*divmod(float(time) * 60, 60))
        current_time = datetime.today()
        expected_time_arrival = current_time + timedelta(hours=int(result_hours), minutes=int(result_mins))
        return f'{expected_time_arrival.strftime("%b %dth, %H:%Mh")}'

    #This function return the value of the distance between two location.
    def check_distance_between_locations(self, start_location, end_location) -> AvailableDestiNation:
            
        for key, value in AvailableDestiNation.ALL_LOCATIONS.items():
                    
            for location_value in value.keys():
                for x in self._check:
                    if location_value == x.start_location:
                        self._total_km_package.append(value.get(end_location))
                        return value.get(end_location)
                    else:
                        self._total_km_package.append(value.get(end_location))
                        return value.get(end_location)

        raise ValueError(
            'Please enter valid location. The current one is not available in Australia.')

    # This function returns the all packages that exist in console.
    def view_all(self, id_search) -> DeliveryPackage:
    
        for current_id in self._check:
            if current_id.unique_ids == id_search[0]:
                return f'Package with ID: {current_id.unique_ids} was created in {current_id.start_location} at ({current_id._date_of_creation}).\nDetailed information will be sent at {current_id.contact}'

        return ValueError("No location with this ID found")

    def assign_truck_to_package(self, aasign_by_id):
        """
        Args:
            assign_by_id : check if this id exist and assign it to free truck
        Raises:
            ValueError: Raising error if no id found
        Returns:
            The assigned truck and truck id
        """        
        for package_check in self._check:
            for key, value_destinations in AvailableDestiNation.ALL_LOCATIONS.items():
                if package_check.unique_ids == aasign_by_id[0]:
                    end_location_km = value_destinations.get(package_check.end_location)
                    if Scania.MAX_CAPACITY > package_check.weight and Scania.MAX_RANGE > end_location_km:
                        Scania.NUMBER_OF_VEHICLES -= 1
                        Scania.VEHICLE_ID += 1
                        if Scania.NUMBER_OF_VEHICLES >= 0:
                            return f'Delivery package was assign to truck: {Scania.TRUCK_NAME} with vehicle ID: {Scania.VEHICLE_ID}'
                    if Man.MAX_CAPACITY > package_check.weight and Man.MAX_RANGE > end_location_km:
                        Man.NUMBER_OF_VEHICLES -= 1
                        Man.VEHICLE_ID += 1
                        if Man.NUMBER_OF_VEHICLES >= 0:
                            return f'Delivery package was assign to truck: {Man.TRUCK_NAME} with vehicle ID: {Man.VEHICLE_ID}'
                    if Actros.MAX_CAPACITY > package_check.weight and Actros.MAX_RANGE > end_location_km:
                        Actros.NUMBER_OF_VEHICLES -= 1
                        Actros.VEHICLE_ID += 1
                        if Actros.NUMBER_OF_VEHICLES >= 0:
                            return f'Delivery package was assign to truck: {Actros.TRUCK_NAME} with vehicle ID: {Actros.VEHICLE_ID}'

                    raise ValueError("No avaible truck found for this criteria!")

    #This function search between location and return the start location and end location + arriving time
    def find_distance_by_location(self, *args) -> DeliveryPackage:
           
        for location_distance in self._check:
            if location_distance.start_location == args[0] and location_distance.end_location == args[1]:
                return f'The package was create in {location_distance.start_location} at ({location_distance._date_of_creation}) and will arrive at {location_distance.end_location} by'
               
        return ValueError("Location not found or written incorrectly! Please try again!")
