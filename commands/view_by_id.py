from core.application_data import ApplicationData

class ViewByIdCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        search_by_id = self._params

        return f'{self._app_data.view_all(search_by_id)}'
