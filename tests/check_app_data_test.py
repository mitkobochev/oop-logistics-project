
import unittest
from datetime import datetime, timedelta

from models.delivery_package import DeliveryPackage
from core.application_data import ApplicationData

class ApplicationDataShould(unittest.TestCase):
    def test_if_check_time_lenght_works_correctly(self):
        #Arrange
        time = 400/87
        time = str(time)
        time.split(".")
        result_hours = '{0:02.0f}'.format(*divmod(float(time) * 60, 60))
        result_mins = '{1:02.0f}'.format(*divmod(float(time) * 60, 60))
        current_time = datetime.today()
        expected_time_arrival = (current_time + timedelta(hours=int(result_hours), minutes=int(result_mins)))
        result = ((expected_time_arrival.strftime("%b %dth, %H:%Mh")))
        #Act
        check_time_lenght = ApplicationData()
        #Assert
        self.assertEqual((result), check_time_lenght.check_time_length(400, 87))
    
    def test_if_return_correct_distance_value(self):
        #Arrange
        distance_one = "Not found"
        distance_two = "Sydney"
        app_date = ApplicationData()

        #Assert & Act
        with self.assertRaises(ValueError):
            command = app_date.check_distance_between_locations(distance_one, distance_two)