from core.application_data import ApplicationData

class CreatePackageItemsCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        pack_id, start_loc, end_loc, weight, contact = self._params
        pack_ids = int(pack_id)
        kg_weight = int(weight)

        self._app_data.create_delivery_package(
            pack_id, start_loc, end_loc, kg_weight, contact)

        return f'Package with ID: {pack_ids} with {kg_weight}kg weight was created'
        