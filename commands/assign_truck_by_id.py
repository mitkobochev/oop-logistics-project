from core.application_data import ApplicationData

class AssignTruckByIdCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        try_id = self._params

        return f'{self._app_data.assign_truck_to_package(try_id)}'