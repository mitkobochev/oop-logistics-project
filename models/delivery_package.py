from datetime import datetime

class DeliveryPackage:
    def __init__(self,unique_ids: int, start_location: str, end_location: str, weight: int, contact: str):
        self._unique_ids = unique_ids
        self._start_location = start_location
        self._end_locatin = end_location
        self._weight = weight
        self._contact = contact
        self._date_of_creation = datetime.today().strftime("%b %dth, %H:%Mh")
        
    @property
    def unique_ids(self):
        return self._unique_ids

    @property
    def start_location(self):
        return self._start_location

    @property
    def end_location(self):
        return self._end_locatin

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value):
        self._weight = value

    @property
    def contact(self):
        return self._contact

    @contact.setter
    def contact(self, value):
        self._contact = value

    def __str__(self):
        return f'{self._unique_ids}, {self._start_location}, {self._date_of_creation}, {self._end_locatin}, {self._weight}, {self._contact}'
