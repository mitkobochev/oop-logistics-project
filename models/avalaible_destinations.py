class AvailableDestiNation:
    SYD = {'Melbourne': 877, 'Adelaide': 1376, 'Alice Springs': 2762, 'Brisbane': 909, 'Darwin': 3935, 'Perth': 4016}
    MEL = {'Sydney': 877, 'Adelaide': 725, 'Alice Springs': 2255, 'Brisbane': 1765, 'Darwin': 3752, 'Perth': 3509}
    ADL = {'Sydney': 1376, 'Melbourne': 725, 'Alice Springs': 1530, 'Brisbane': 1927, 'Darwin': 3027, 'Perth': 2785}
    ASP = {'Sydney': 2762, 'Melbourne': 2255, 'Adelaide': 1530, 'Brisbane': 2993, 'Darwin': 1497, 'Perth': 2481}
    BRI = {'Sydney': 909, 'Melbourne': 1765, 'Adelaide': 1927, 'Alice Springs': 2993, 'Darwin': 3426, 'Perth': 4311}
    DAR = {'Sydney': 3935, 'Melbourne': 3752, 'Adelaide': 3027, 'Alice Springs': 1497, 'Brisbane': 3426, 'Perth': 4025}
    PER = {'Sydney': 4016, 'Melbourne': 3509, 'Adelaide': 2785, 'Alice Springs': 2481, 'Brisbane': 4311, 'Darwin': 4025}
    ALL_LOCATIONS = {'Sydney': SYD, 'Melbourne': MEL, 'Adelaide': ADL, 'Alice springs': ASP, 'Brisbane': BRI, 'Darwin': DAR, 'Perth': PER}
    