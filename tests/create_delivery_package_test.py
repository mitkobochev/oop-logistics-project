import unittest
from models.delivery_package import DeliveryPackage

class CreateDeliveryPackageShould(unittest.TestCase):
    def test_init_setProperties(self):
        #Arrange
        valid_id = 1
        valid_start_location = "Melbourne"
        valid_end_location = "Sydney"
        valid_weight = 500
        valid_contact = "test_email@icloud.com"
        #Act
        delivery_package_check = DeliveryPackage(1, "Melbourne", "Sydney", 500, "test_email@icloud.com")
        #Assert
        self.assertEqual(valid_id, delivery_package_check.unique_ids)
        self.assertEqual(valid_start_location, delivery_package_check.start_location)
        self.assertEqual(valid_end_location, delivery_package_check.end_location)
        self.assertEqual(valid_weight, delivery_package_check.weight)
        self.assertEqual(valid_contact, delivery_package_check.contact)
    
