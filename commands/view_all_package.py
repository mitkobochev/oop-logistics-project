from core.application_data import ApplicationData

class ViewAllCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        if len(self._app_data._check) == 0:
            return 'No packages found in the system'
        else:
            return '\n'.join(
                [f'System found {len(self._app_data._check)} records!'] +
                [f'Package with ID: {group.unique_ids} was placed at {group.start_location} and should arrive at {group.end_location}' for group in self._app_data._check])
