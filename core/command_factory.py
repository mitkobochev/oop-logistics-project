from commands.search_route_by_location import SearchRouteByLocationCommand
from commands.create_package_items import CreatePackageItemsCommand
from commands.view_by_id import ViewByIdCommand
from commands.view_all_package import ViewAllCommand
from commands.assign_truck_by_id import AssignTruckByIdCommand


class CommandFactory:
    def __init__(self, data):
        self._app_data = data

    def create(self, input_line: str):
        cmd, *params = input_line.split()

        if cmd.lower() == "searchroute":
            return SearchRouteByLocationCommand(params, self._app_data)
        if cmd.lower() == "createpackage":
            return CreatePackageItemsCommand(params, self._app_data)
        if cmd.lower() == "customeridcheck":
            return ViewByIdCommand(params, self._app_data)
        if cmd.lower() == "viewsystem":
            return ViewAllCommand(params, self._app_data)
        if cmd.lower() == "assigntruckpackgeid":
            return AssignTruckByIdCommand(params, self._app_data)

        raise ValueError('Invalid command name')
