from core.application_data import ApplicationData
from core.command_factory import CommandFactory
from core.engine import Engine

app_data = ApplicationData()
cmd_factory = CommandFactory(app_data)
engine = Engine(cmd_factory)

engine.start()  

# Available Input:
#-------------------------------------------
""""
createpackage 1 Melbourne Adelaide 100 test_email@gmail.com
createpackage 2 Darwin Perth 5000 ivan_ivanov@abv.bg
searchroute Melbourne Adelaide
searchroute Darwin Perth
customeridcheck 2
customeridcheck 1
viewsystem
assigntruckpackgeid 1
assigntruckpackgeid 2
"""