from core.application_data import ApplicationData


class SearchRouteByLocationCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        start_location, end_location = self._params
        try:
            check_distance_between = self._app_data.check_distance_between_locations(
                start_location, end_location)
            expected_time_of_arriving = self._app_data.check_time_length(
                check_distance_between, avg_speed=87.0)
            location_names = f'{self._app_data.find_distance_by_location(start_location, end_location)}'

            return f'{location_names} ({expected_time_of_arriving})'
        except ValueError as error:
            return error
